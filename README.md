# Space Explorer

Welcome to the Space Explorer web app. The app is deployed [here](https://spaceexplorer2-6d63e.firebaseapp.com/).
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Getting Started

Run `npm install` in the project directory to install dependencies

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about [deployment](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#deployment) for more information.

### API Documentation

API Documentation can be found [here](https://spacesapi-b5d05.firebaseapp.com/docs/).

### Mock API & Services

A mock API has been deployed [here](https://spacesapi-b5d05.firebaseapp.com/docs/).

If the mock API service is down, it can be found [here](https://bitbucket.org/courier6/spaces-api) for local deployment.
If deployed locally, make sure to switch the host to `http://localhost:8080/api` in APIUtil.js under the space-explorer source directory like so:
```
class APIUtil {
    static getHost(){
        ///const host = 'https://spacesapi-b5d05.firebaseapp.com/api';
        const host = 'http://localhost:8080/api';
        return host;
    }
}
```

