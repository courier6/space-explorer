import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import NavigationRoutes from './NavigationRoutes';

Enzyme.configure({adapter: new Adapter()});

jest.mock('react-dom');

it('renders without crashing', () => {
    const div = document.createElement('div');
    const wrapper = shallow(<NavigationRoutes/>);

    ReactDOM.render(
        wrapper
       , div);
});
