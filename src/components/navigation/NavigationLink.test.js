import React from 'react';
import ReactDOM from 'react-dom';
import NavigationLink from './NavigationLink';

import { MemoryRouter } from 'react-router-dom';


it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
        <MemoryRouter>
            <NavigationLink name="test" path="test"/>
        </MemoryRouter>, div);
});
