import React from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import base64 from 'base-64';
import SpaceContainer from '../space/SpaceContainer';

const NavigationRoutes = ({routeData = []}) => (
    <div className="navigation-routes">
        <Switch>
            <Route exact path='/' render={() => <SpaceContainer spaceContainerData={routeData[0]}/>}/>
            {routeData.map((routeDataObj) => {
                    let key = 'route' + routeDataObj.spaceId;
                    let path = '/' + routeDataObj.spaceId;

                    return <Route key={key} path={path}
                                  render={() =>
                                      <SpaceContainer spaceContainerData={routeDataObj}/>
                                  }
                    />
                }
            )}
            {/*The following route is used to render the base64 text*/}
            <Route path='/:text'
                   render={(props) => <div>{base64.decode(props.match.params.text)}</div>}/>
        </Switch>
    </div>
);

NavigationRoutes.propTypes = {
    routeData: PropTypes.array,
};

export default NavigationRoutes;
