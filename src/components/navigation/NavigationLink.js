import React  from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import './NavigationLink.css';

const NavigationLink = ({path, name}) => (
    <li className="navigation-link">
        <NavLink exact activeClassName="selected" to={path}>{name}</NavLink>
    </li>
);

NavigationLink.propTypes = {
    path: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
};

export default NavigationLink;
