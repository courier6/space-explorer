import React from 'react';
import PropTypes from 'prop-types';
import './Navigation.css';
import NavigationLink from './NavigationLink';

// Dynamically generates navigation tabs
const Navigation = ({tabData = []}) => (
    <ul className="nav nav-pills nav-stacked">
        {tabData.map((tabObj, index) =>
            <NavigationLink name={tabObj.spaceTitle} path={'/' + tabObj.spaceId} key={tabObj.spaceId}/>
        )}
    </ul>
);

Navigation.propTypes = {
    tabData: PropTypes.array,
};

export default Navigation;
