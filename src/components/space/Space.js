import React from 'react';
import PropTypes from 'prop-types';
import SpaceTabs from './SpaceTabs';
import './Space.css'

const Space = ({assetsData, description, entriesData, title, userName, errorFetchingTableData}) => (
    <div className="space">
        <h1>{title} <span>created by {userName}</span></h1>
        {errorFetchingTableData ?
            (
                <p>Not Available</p>
            )
            :
            (
                <div>
                    <p className="description">{description}</p>
                    <SpaceTabs entries={entriesData} assets={assetsData}/>
                </div>
            )
        }
    </div>
);

Space.propTypes = {
    assetsData: PropTypes.array,
    description: PropTypes.string,
    entriesData: PropTypes.array,
    title: PropTypes.string,
    userName: PropTypes.string,
    errorFetchingTableData: PropTypes.bool,
};

export default Space;
