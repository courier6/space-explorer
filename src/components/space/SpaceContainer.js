import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Space from './Space';
import SpaceService from "../../apiService/SpaceService";
import UserService from '../../apiService/UserService';

// SpacesContainer contains spaces
class SpaceContainer extends Component {
    state = {
        entriesData: undefined,
        assetsData: undefined,
        userName: '',
        errorFetchingTableData: false,
    };

    componentDidMount() {

        // helper method updates state when error is found
        const errorHelper = error =>
            this.setState({
                errorFetchingTableData: true
            });

        SpaceService.getSpaceEntriesById(this.props.spaceContainerData.spaceId).then(data =>
            this.setState({
                entriesData: data.items
            })
        ).catch(errorHelper);

        SpaceService.getSpaceAssetsById(this.props.spaceContainerData.spaceId).then(data =>
            this.setState({
                assetsData: data.items
            })
        ).catch(errorHelper);

        UserService.getUserById(this.props.spaceContainerData.spaceCreatedBy).then(data => {
            this.setState({
                userName: (data !== null ? data.fields.name : "Name Unavailable")
            });
        });
    }


    render() {
        if (!this.state.entriesData || !this.state.assetsData) {
            if (!this.state.errorFetchingTableData) {
                return <p>Loading...</p>;
            }
        }

        return (
            <div className="space-container">
                <Space
                    title={this.props.spaceContainerData.spaceTitle}
                    description={this.props.spaceContainerData.spaceDescription}
                    entriesData={this.state.entriesData}
                    assetsData={this.state.assetsData}
                    userName={this.state.userName}
                    errorFetchingTableData={this.state.errorFetchingTableData}
                />
            </div>
        );
    }
}


SpaceContainer.propTypes = {
    spaceContainerData: PropTypes.shape({
        spaceId: PropTypes.string,
        spaceTitle: PropTypes.string,
        spaceDescription: PropTypes.string,
        spaceCreatedBy: PropTypes.string,
    })
};

export default SpaceContainer;
