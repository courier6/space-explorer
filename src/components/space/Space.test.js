import React from 'react';
import ReactDOM from 'react-dom';
import Space from './Space';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Space
        errorFetchingTableData={true}
        entriesData={null}
        assetsData={null}
        title="test"
        username="test"
        description="test"
    />, div);
});
