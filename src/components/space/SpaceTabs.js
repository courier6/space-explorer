import React, { Component } from 'react';
import PropTypes from 'prop-types';
import UserService from '../../apiService/UserService';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import { Table, Column, Cell } from 'fixed-data-table-2';
import 'fixed-data-table-2/dist/fixed-data-table.css';
import './SpaceTabs.css';

// SpaceTabs will contain assets and entries tabs/tables
class SpaceTabs extends Component {

    state = {
        entriesRowData: [],
        assetsRowData: [],
        colSortDirs: {
            entries: {
                title: 'ASC',
                summary: 'ASC',
                createdBy: 'ASC',
                updatedBy: 'ASC',
                lastUpdated: 'ASC'
            },
            assets: {
                title: 'ASC',
                contentType: 'ASC',
                fileName: 'ASC',
                createdBy: 'ASC',
                updatedBy: 'ASC',
                lastUpdated: 'ASC'
            }
        }
    };

    formatDate = input => {
        let date = new Date(input);

        let result = date.toLocaleString('en-us', {year: 'numeric', month: 'numeric', day: 'numeric'});

        return result.replace(/\//gi, '-');
    };

    componentDidMount() {
        this.getEntriesRowData();
        this.getAssetsRowData();
    }

    getEntriesRowData = async () => {
        let entries = this.props.entries;
        let entriesRowDataTemp = [];

        for (const entry of entries) {
            let row = {
                title: entry.fields.title,
                summary: entry.fields.summary,
                lastUpdated: this.formatDate(entry.sys.updatedAt),
                body: entry.fields.body
            };

            await UserService.getUserById(entry.sys.createdBy).then((data) => {
                row['createdBy'] = (data !== null ? data.fields.name : "Unavailable");
            });

            await UserService.getUserById(entry.sys.updatedBy).then((data) => {
                row['updatedBy'] = (data !== null ? data.fields.name : "Unavailable");
            });

            entriesRowDataTemp.push(row);
        }

        // set entriesRowData;
        this.setState({
            entriesRowData: entriesRowDataTemp
        });

    };


    getAssetsRowData = async () => {
        let assets = this.props.assets;
        let assetsRowDataTemp = [];

        for (const asset of assets) {
            let row = {
                title: asset.fields.title,
                contentType: asset.fields.contentType,
                fileName: asset.fields.fileName,
                lastUpdated: this.formatDate(asset.sys.updatedAt),
                upload: asset.fields.upload
            };
            await UserService.getUserById(asset.sys.createdBy).then((data) => {
                row['createdBy'] = (data !== null ? data.fields.name : "Unavailable");
            });

            await UserService.getUserById(asset.sys.updatedBy).then((data) => {
                row['updatedBy'] = (data !== null ? data.fields.name : "Unavailable");
            });

            assetsRowDataTemp.push(row);
        }

        this.setState({
            assetsRowData: assetsRowDataTemp
        });
    };

    sort = (section, sortBy) => {
        //compare helper method
        const compare = (a, b) => {
            const rowA = a[sortBy].toUpperCase();
            const rowB = b[sortBy].toUpperCase();

            let comparison = 0;

            if (rowA > rowB) {
                comparison = 1;
            } else if (rowA < rowB) {
                comparison = -1;
            }

            if (this.state.colSortDirs[section][sortBy] === 'DESC') {
                return comparison * -1;
            } else {
                return comparison;
            }
        };

        if (section === 'entries') {
            let entriesRowDataTemp = JSON.parse(JSON.stringify(this.state.entriesRowData));

            this.setState({
                entriesRowData: entriesRowDataTemp.sort(compare)
            });

        } else if (section === 'assets') {
            let assetsRowDataTemp = JSON.parse(JSON.stringify(this.state.assetsRowData));

            this.setState({
                assetsRowData: assetsRowDataTemp.sort(compare)
            });
        }

        let colSortDirTemp = JSON.parse(JSON.stringify(this.state.colSortDirs));

        colSortDirTemp[section][sortBy] = (this.state.colSortDirs[section][sortBy] === 'ASC' ? 'DESC' : 'ASC')

        this.setState({
            colSortDirs: colSortDirTemp
        });
    };

    render() {

        if (this.state.entriesRowData.length === 0 || this.state.assetsRowData.length === 0) {
            return <p>Loading...</p>
        }

        const entriesTable = (<Table
            rowHeight={100}
            rowsCount={3}
            width={900}
            height={354}
            headerHeight={50}>
            <Column
                header={<Cell onClick={() => this.sort('entries', 'title')}>
                    Title <span
                    className={this.state.colSortDirs.entries.title === 'ASC' ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down'}/></Cell>}
                cell={<MyCell data={this.state.entriesRowData} field="title" linkTo="body"/>}
                width={100}
            />
            <Column
                header={<Cell onClick={() => this.sort('entries', 'summary')}>
                    Summary <span
                    className={this.state.colSortDirs.entries.summary === 'ASC' ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down'}/></Cell>}
                cell={<MyCell data={this.state.entriesRowData} field="summary" linkTo="body"/>}
                width={300}
            />
            <Column
                header={<Cell onClick={() => this.sort('entries', 'createdBy')}>
                    Created By <span
                    className={this.state.colSortDirs.entries.createdBy === 'ASC' ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down'}/></Cell>}
                cell={<MyCell data={this.state.entriesRowData} field="createdBy" linkTo="body"/>}
                width={200}
            />
            <Column
                header={<Cell onClick={() => this.sort('entries', 'updatedBy')}>
                    Updated By <span
                    className={this.state.colSortDirs.entries.updatedBy === 'ASC' ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down'}/></Cell>}
                cell={<MyCell data={this.state.entriesRowData} field="updatedBy" linkTo="body"/>}
                width={150}
            />
            <Column
                header={<Cell onClick={() => this.sort('entries', 'lastUpdated')}>
                    Last Updated <span
                    className={this.state.colSortDirs.entries.lastUpdated === 'ASC' ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down'}/></Cell>}
                cell={<MyCell data={this.state.entriesRowData} field="lastUpdated" linkTo="body"/>}
                width={150}
            />
        </Table>);


        const assetsTable = (<Table
            rowHeight={100}
            rowsCount={3}
            width={900}
            height={354}
            headerHeight={50}>
            <Column
                header={<Cell onClick={(e) => this.sort('assets', 'title')}>
                    Title <span
                    className={this.state.colSortDirs.assets.title === 'ASC' ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down'}/>
                </Cell>}
                cell={<MyCell data={this.state.assetsRowData} field="title" linkTo="upload"/>}
                width={200}
            />
            <Column
                header={<Cell onClick={() => this.sort('assets', 'contentType')}>
                    Content Type <span
                    className={this.state.colSortDirs.assets.contentType === 'ASC' ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down'}/>
                </Cell>}
                cell={<MyCell data={this.state.assetsRowData} field="contentType" linkTo="upload"/>}
                width={125}
            />
            <Column
                header={<Cell onClick={() => this.sort('assets', 'fileName')}>
                    File Name <span
                    className={this.state.colSortDirs.assets.fileName === 'ASC' ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down'}/></Cell>}
                cell={<MyCell data={this.state.assetsRowData} field="fileName" linkTo="upload"/>}
                width={200}
            />
            <Column
                header={<Cell onClick={() => this.sort('assets', 'createdBy')}>
                    Created By <span
                    className={this.state.colSortDirs.assets.createdBy === 'ASC' ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down'}/></Cell>}
                cell={<MyCell data={this.state.assetsRowData} field="createdBy" linkTo="upload"/>}
                width={125}
            />
            <Column
                header={<Cell onClick={() => this.sort('assets', 'updatedBy')}>
                    Updated By <span
                    className={this.state.colSortDirs.assets.updatedBy === 'ASC' ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down'}/></Cell>}
                cell={<MyCell data={this.state.assetsRowData} field="updatedBy" linkTo="upload"/>}
                width={150}
            />
            <Column
                header={<Cell onClick={() => this.sort('assets', 'lastUpdated')}>
                    Last Updated <span
                    className={this.state.colSortDirs.assets.lastUpdated === 'ASC' ? 'glyphicon glyphicon-arrow-up' : 'glyphicon glyphicon-arrow-down'}/></Cell>}
                cell={<MyCell data={this.state.assetsRowData} field="lastUpdated" linkTo="upload"/>}
                width={100}
            />
        </Table>);


        return (
            <div className="space-tabs">
                <Tabs>
                    <TabList>
                        <Tab>Entries</Tab>
                        <Tab>Assets</Tab>
                    </TabList>

                    <TabPanel>
                        {entriesTable}
                    </TabPanel>
                    <TabPanel>
                        {assetsTable}
                    </TabPanel>
                </Tabs>
            </div>

        );
    }
}

SpaceTabs.propTypes = {
    entries: PropTypes.array,
    assets: PropTypes.array,
};


// helper class
class MyCell extends React.Component {
    render() {
        const {rowIndex, data, field, linkTo, ...props} = this.props;

        return (
            <Cell {...props}>
                <a href={data[rowIndex][linkTo]}>{data[rowIndex][field]}</a>
            </Cell>
        );
    }
}

export default SpaceTabs;
