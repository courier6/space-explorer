import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import SpaceContainer from './SpaceContainer';
import UserService from "../../apiService/UserService";
import SpaceService from "../../apiService/SpaceService";

Enzyme.configure({adapter: new Adapter()});

jest.mock('react-dom');

describe('SpaceExplorer tests', () => {
    beforeEach(function () {
        SpaceService.getSpaceEntriesById = jest.fn(() => Promise.resolve({}));
        SpaceService.getSpaceAssetsById = jest.fn(() => Promise.resolve({}));
        UserService.getUserById = jest.fn(() => Promise.resolve({
            fields: {
                name: 'Alana'
            }
        }));

    });

    it('renders without crashing', () => {
        const div = document.createElement('div');
        const routeDataObj = {
            spaceId: "yadj1kx9rmg01",
            spaceTitle: "My Second Space",
            spaceDescription: "",
            spaceCreatedBy: "4FLrUHftHW3v2BLi9fzfjU"
        };

        const wrapper = shallow(<SpaceContainer spaceContainerData={routeDataObj}/>);
        ReactDOM.render(
            wrapper
            , div);
    });
});