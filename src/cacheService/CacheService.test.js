import CacheService from './CacheService';
import 'whatwg-fetch';

let localStorage = (() => {
    return {
        getItem: undefined, //these will return a mocked response as show in beforeEach()
        removeItem: undefined,
        setItem: undefined
    }
});

global.localStorage = localStorage;

const mockStoredValue = "{\"http://example.com/api/spaces\": \"data\"}";
const mockStoredValueNull = null;

const mockSuccessfulResponse = {
    ok: true,
    status: 200,
    json: () => {
        return {total: 1}
    },
    clone: () => {
        return new Response(JSON.stringify({test: 1}));
    }
};

const mockFailureResponse = {
    ok: false,
    status: 404,
    json: () => {
        return {}
    }
};


describe('CacheService tests', () => {

    beforeEach(() => {
        localStorage.removeItem = jest.fn();
        localStorage.setItem = jest.fn();
        fetch = jest.fn(() => Promise.resolve(mockSuccessfulResponse));

    });

    it('get should call localStorage.getItem() with url argument as the key', () => {
        localStorage.getItem = jest.fn().mockReturnValue(mockStoredValue);

        const url = 'http://example.com/api/spaces';

        CacheService.get(url);

        expect(localStorage.getItem).toBeCalledWith(url);
    });

    it('get should not find the url in the localStorage which means Date.now will never be invoked', () => {
        localStorage.getItem = jest.fn().mockReturnValue(mockStoredValueNull);
        Date.now = jest.fn();

        const url = 'http://example.com/api/spaces';

        CacheService.get(url);

        expect(Date.now).toHaveBeenCalledTimes(0);
    });
});