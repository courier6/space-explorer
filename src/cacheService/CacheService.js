import 'whatwg-fetch';

class CacheService {
    static get = url => {
        const cachedResponseString = localStorage.getItem(url);
        const cachedResult = JSON.parse(cachedResponseString);

        // if there's a cached response, return it unless
        // it's expired. In that case, remove it from localStorage
        // and proceed with fetching from the server
        if (cachedResult !== null) {
            const expires = cachedResult['expires'];

            if (Date.now() < expires) {
                const response = new Response(JSON.stringify(cachedResult));
                return Promise.resolve(response);
            } else {
                //remove expired value by key
                localStorage.removeItem(url);
            }
        }

        // fetch a fresh response and store it in cache
        return fetch(url).then(response => {
            if (response.status === 200) {
                response.clone().json().then((data) => {
                    // expire local cache in 1 minute
                    data['expires'] = Date.now() + 1000 * 60 * 1;

                    localStorage.setItem(url, JSON.stringify(data));
                });
            }

            return response;
        })
    }
}

export default CacheService;
