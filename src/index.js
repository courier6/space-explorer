import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import SpaceExplorer from './SpaceExplorer';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter } from 'react-router-dom';

ReactDOM.render((
    <BrowserRouter>
        <SpaceExplorer/>
    </BrowserRouter>
), document.getElementById('root'));
registerServiceWorker();
