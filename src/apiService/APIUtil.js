class APIUtil {
    static getHost(){
        const host = 'https://spacesapi-b5d05.firebaseapp.com/api';
        // const host = 'http://localhost:8080/api';
        return host;
    }
}

export default APIUtil;
