import React from 'react';
import Enzyme  from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import UserService from './UserService';
import CacheService from '../cacheService/CacheService';

Enzyme.configure({adapter: new Adapter()});

const mockSuccessfulResponse = {
    ok: true,
    status: 200,
    json: () => {
        return {
            "fields": {
                "name": "Alana Atlassian",
                "role": "Author"
            },
            "sys": {
                "id": "4FLrUHftHW3v2BLi9fzfjU",
                "type": "User"
            }
        }
    }
};

const mockFailureResponse = {
    ok: false,
    status: 404,
    json: () => {
        return {}
    }
};

describe('UserService tests', () => {
    beforeEach(() => {

    });

    it('should return with no error', () => {
        CacheService.get = jest.fn(() => Promise.resolve(mockSuccessfulResponse));

        UserService.getUserById('4FLrUHftHW3v2BLi9fzfjU').then((data) => {
            expect(data.fields.name).toEqual('Alana Atlassian');
        });
    })

    it('should throw an error', () => {
        CacheService.get = jest.fn(() => Promise.reject(mockFailureResponse));
        console.log = jest.fn();

        UserService.getUserById('123').then((data) => {
            expect(console.log).toHaveBeenCalled();
        });
    })
});