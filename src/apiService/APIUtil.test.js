import APIUtil from './APIUtil';

describe('APIUtil tests', () => {

    it('should return the localhost url', () => {
        const host = 'https://spacesapi-b5d05.firebaseapp.com/api';
        // const host = 'http://localhost:8080/api';
        expect(APIUtil.getHost()).toEqual(host);
    })

});