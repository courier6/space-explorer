import APIUtil from './APIUtil';
import 'whatwg-fetch';
import CacheService from '../cacheService/CacheService';

class UserService {

    static getUserById(userId) {
        return CacheService.get(APIUtil.getHost() + '/users/' + userId)
            .then(function (response) {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response.json()
            }).catch(function (error) {
                console.log('Error in getUsersById() ', error);
                return null;
            });
    }
}

export default UserService;
