import APIUtil from './APIUtil';
import 'whatwg-fetch';
import CacheService from '../cacheService/CacheService';

class SpaceService {

    static getSpaces() {
        return CacheService.get(APIUtil.getHost() + '/space')
            .then(function (response) {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response.json()
            }).catch(function (error) {
                console.log('Error in getSpaces() ', error);
                return null;
            });
    }

    static getSpaceEntriesById(spaceId) {
        return CacheService.get(APIUtil.getHost() + '/space/' + spaceId + '/entries')
            .then(function (response) {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response.json()
            }).catch(function (error) {
                console.log('Error in getSpaceEntriesById() ', error);
                return null;
            });
    }

    static getSpaceAssetsById(spaceId) {
        return CacheService.get(APIUtil.getHost() + '/space/' + spaceId + '/assets')
            .then(function (response) {
                if (!response.ok) {
                    throw Error(response.statusText);
                }
                return response.json()
            }).catch(function (error) {
                console.log('Error in getSpaceAssetsById() ', error);
                return null;
            });
    }
}

export default SpaceService;
