import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SpaceService from './SpaceService';
import CacheService from '../cacheService/CacheService';

Enzyme.configure({adapter: new Adapter()});

const mockSuccessfulResponse = {
    ok: true,
    status: 200,
    json: () => {
        return {total: 1}
    }
};

const mockFailureResponse = {
    ok: false,
    status: 404,
    json: () => {
        return {}
    }
};

describe('SpaceService tests', () => {
    beforeEach(() => {

    });

    it('should return with no error', () => {
        CacheService.get = jest.fn(() => Promise.resolve(mockSuccessfulResponse));

        SpaceService.getSpaces().then((data) => {
            expect(data.total).toEqual(1);
        });
    })

    it('should throw an error', () => {
        CacheService.get = jest.fn(() => Promise.reject(mockFailureResponse));
        console.log = jest.fn();

        SpaceService.getSpaces().then((data) => {
            expect(console.log).toHaveBeenCalled();
        });
    })
});