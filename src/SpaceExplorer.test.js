import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import SpaceExplorer from './SpaceExplorer';
import SpaceService from './apiService/SpaceService';

Enzyme.configure({adapter: new Adapter()});

jest.mock('react-dom');

const mockData = {
    "sys": {"type": "Array"},
    "total": 1,
    "skip": 0,
    "limit": 100,
    "items": [{
        "fields": {
            "title": "My First Space",
            "description": "This space was created by Alanna Atlassian as an example of how to create a space and relate assets and entries to it."
        },
        "sys": {
            "id": "yadj1kx9rmg0",
            "type": "Space",
            "createdAt": "2015-05-18T11:29:46.809Z",
            "createdBy": "4FLrUHftHW3v2BLi9fzfjU",
            "updatedAt": "2015-05-18T11:29:46.809Z",
            "updatedBy": "4FLrUHftHW3v2BLi9fzfjU"
        }
    }]
};

describe('SpaceExplorer tests', () => {
    beforeEach(() => {
        SpaceService.getSpaces = jest.fn(() => Promise.resolve(mockData));
    });

    it('renders without crashing', () => {

        const div = document.createElement('div');
        const wrapper = shallow(<SpaceExplorer/>);
        ReactDOM.render(
            wrapper
            , div);
    });

    it('has a default state', () => {
        const wrapper = shallow(<SpaceExplorer/>);
        expect(wrapper.state()).toEqual({
            spaceData: undefined,
            errorFetchingData: false
        })
    });

    it('renders with a paragraph container a loading message', () => {
        const wrapper = shallow(<SpaceExplorer/>);
        expect(wrapper.contains(<p className="App">Loading....</p>)).toEqual(true);
    });

    it('should have changed state after mockData was passed', (done) => {
        const wrapper = shallow(<SpaceExplorer/>);

        setImmediate(() => {
            const spaceDataExpectation = {
                "routeData": [{
                    "spaceCreatedBy": "4FLrUHftHW3v2BLi9fzfjU",
                    "spaceDescription": "This space was created by Alanna Atlassian as an example of how to create a space and relate assets and entries to it.",
                    "spaceId": "yadj1kx9rmg0",
                    "spaceTitle": "My First Space"
                }], "tabData": [{"spaceId": "yadj1kx9rmg0", "spaceTitle": "My First Space"}]
            };
            expect(wrapper.state().spaceData).toEqual(spaceDataExpectation);
            done();
        })
    });

    it('should reach return div.App after mockData was passed', (done) => {
        const wrapper = shallow(<SpaceExplorer/>);

        setImmediate(() => {
            wrapper.update(); //reload since state has been updated
            const result =  wrapper.find('div.App').length;
            expect(result).toEqual(1);
            done();
        })
    });
});
