import React, { Component } from 'react';
import './SpaceExplorer.css';
import NavigationRoutes from "./components/navigation/NavigationRoutes";
import SpaceService from './apiService/SpaceService';
import Navigation from "./components/navigation/Navigation";

class SpaceExplorer extends Component {

    state = {
        spaceData: undefined,
        errorFetchingData: false
    };

    componentDidMount() {
        SpaceService.getSpaces().then((data) => {
            // massage response json data
            let spaceDataTemp = {tabData: [], routeData: []};
            // get data for Navigation and NavigationRoutes
            for (let i = 0; i < data.items.length; i++) {
                spaceDataTemp['tabData'].push({
                    spaceId: data.items[i].sys.id,
                    spaceTitle: data.items[i].fields.title
                });

                spaceDataTemp['routeData'].push({
                    spaceId: data.items[i].sys.id,
                    spaceTitle: data.items[i].fields.title,
                    spaceDescription: data.items[i].fields.description,
                    spaceCreatedBy: data.items[i].sys.createdBy
                });
            }

            this.setState({
                spaceData: spaceDataTemp
            });
        }).catch((error) => {
            this.setState({
                errorFetchingData: true
            })
        });
    }

    render() {

        // if spaceData is undefined and there hasn't been an error fetching data return loading message
        if (!this.state.spaceData) {
            if (!this.state.errorFetchingData) {
                return <p className="App">Loading....</p>
            } else {
                return <p className="App">Whoops! We weren't able to load any Spaces. Please contact your admin.</p>
            }
        }

        return (
            <div className="App">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xs-2">
                            <Navigation tabData={this.state.spaceData.tabData}/>
                        </div>
                        <div className="col-xs-10">
                            <NavigationRoutes routeData={this.state.spaceData.routeData}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SpaceExplorer;
